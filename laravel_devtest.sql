-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2020 at 08:27 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_devtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `csv_data`
--

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL,
  `csv_filename` varchar(255) DEFAULT NULL,
  `csv_header` tinyint(1) DEFAULT NULL,
  `csv_data` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_06_11_052427_create_bin_service', 1),
(3, '2018_06_11_053118_create_bin_service_updates', 1),
(4, '2018_06_11_053255_create_bin_type', 1),
(5, '2018_06_11_054938_create_supplier_table', 1),
(6, '2018_06_11_055545_create_service_area', 1),
(7, '2018_06_11_055941_create_size_table', 1),
(8, '2018_06_11_060117_create_order_service_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinnondelivery`
--

CREATE TABLE `tblbinnondelivery` (
  `idNonDeliveryDays` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `idSupplier` int(10) DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL,
  `idBinType` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbinnondelivery`
--

INSERT INTO `tblbinnondelivery` (`idNonDeliveryDays`, `date`, `idSupplier`, `idUser`, `idBinType`) VALUES
(21, '2019-04-06', 11, 5, 5),
(22, '2019-04-07', 11, 5, 5),
(23, '2019-04-13', 11, 5, 5),
(24, '2019-04-14', 11, 5, 5),
(221, '2019-10-12', 11, 5, 1),
(222, '2019-10-12', 11, 5, 2),
(223, '2019-10-12', 11, 5, 3),
(224, '2019-10-12', 11, 5, 4),
(225, '2019-10-12', 11, 5, 5),
(226, '2019-10-13', 11, 5, 1),
(227, '2019-10-13', 11, 5, 2),
(228, '2019-10-13', 11, 5, 3),
(229, '2019-10-13', 11, 5, 4),
(230, '2019-10-13', 11, 5, 5),
(231, '2019-10-19', 11, 5, 1),
(232, '2019-10-19', 11, 5, 2),
(233, '2019-10-19', 11, 5, 3),
(234, '2019-10-19', 11, 5, 4),
(235, '2019-10-19', 11, 5, 5),
(236, '2019-10-20', 11, 5, 1),
(237, '2019-10-20', 11, 5, 2),
(238, '2019-10-20', 11, 5, 3),
(239, '2019-10-20', 11, 5, 4),
(240, '2019-10-20', 11, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservice`
--

CREATE TABLE `tblbinservice` (
  `idBinService` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `idBinType` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `default_stock` int(11) NOT NULL,
  `idBinSize` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinservice`
--

INSERT INTO `tblbinservice` (`idBinService`, `idSupplier`, `idBinType`, `price`, `stock`, `default_stock`, `idBinSize`) VALUES
(22, 11, 1, 130.00, 10, 10, 1),
(23, 11, 1, 130.00, 5, 5, 2),
(24, 11, 2, 130.00, 4, 10, 1),
(25, 11, 4, 250.00, 0, 2, 1),
(26, 11, 5, 44.00, 8, 10, 1),
(57, 11, 4, 222.00, 0, 1, 2),
(166, 11, 4, 0.00, 0, 0, 6),
(168, 11, 5, 0.00, 0, 0, 2),
(169, 11, 5, 121.00, 0, 1, 3),
(170, 11, 5, 128.00, 0, 1, 4),
(171, 11, 2, 150.00, 5, 10, 2),
(172, 11, 2, 130.00, 9, 10, 3),
(173, 11, 2, 130.00, 10, 10, 5),
(174, 11, 2, 130.00, 8, 8, 6),
(175, 11, 1, 130.00, 7, 10, 6),
(176, 11, 1, 130.00, 8, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceoptions`
--

CREATE TABLE `tblbinserviceoptions` (
  `idBinServiceOptions` int(12) NOT NULL,
  `idUser` int(12) DEFAULT NULL,
  `idSupplier` int(12) DEFAULT NULL,
  `idBinType` int(12) DEFAULT NULL,
  `extraHireagePrice` bigint(20) DEFAULT NULL,
  `extraHireageDays` int(11) DEFAULT NULL,
  `excessWeightPrice` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservicestok`
--

CREATE TABLE `tblbinservicestok` (
  `idBinServiceStok` int(12) NOT NULL,
  `idBinService` int(12) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceupdates`
--

CREATE TABLE `tblbinserviceupdates` (
  `idBinServiceUpdates` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinserviceupdates`
--

INSERT INTO `tblbinserviceupdates` (`idBinServiceUpdates`, `idBinService`, `price`, `stock`, `date`) VALUES
(1024, 22, 130.00, 0, '2020-02-15'),
(1025, 22, 130.00, 9, '2020-02-16'),
(1026, 23, 130.00, 4, '2020-02-17'),
(1027, 23, 130.00, 3, '2020-02-18'),
(1028, 23, 130.00, 3, '2020-02-19'),
(1029, 23, 130.00, 2, '2020-02-20'),
(1030, 23, 130.00, 2, '2020-02-21'),
(1031, 23, 130.00, 1, '2020-02-15'),
(1032, 23, 130.00, 1, '2020-02-16'),
(1033, 24, 130.00, 0, '2020-02-24'),
(1034, 24, 130.00, 0, '2020-02-25'),
(1035, 23, 130.00, 0, '2020-03-02'),
(1036, 23, 130.00, 0, '2020-03-03'),
(1037, 24, 130.00, 1, '2020-02-15'),
(1038, 24, 130.00, 1, '2020-02-16'),
(1039, 24, 130.00, 1, '2020-02-17'),
(1040, 24, 130.00, 1, '2020-02-18'),
(1041, 24, 130.00, 5, '2020-02-19'),
(1042, 24, 130.00, 5, '2020-02-20'),
(1043, 24, 130.00, 1, '2020-02-21'),
(1044, 24, 130.00, 1, '2020-02-22'),
(1045, 24, 130.00, 1, '2020-02-23'),
(1046, 24, 130.00, 1, '2020-02-26'),
(1047, 24, 130.00, 1, '2020-02-27'),
(1048, 24, 130.00, 1, '2020-02-28'),
(1049, 24, 130.00, 9, '2020-03-11'),
(1050, 24, 130.00, 9, '2020-03-12'),
(1051, 171, 150.00, 9, '2020-03-09'),
(1052, 171, 150.00, 9, '2020-03-10'),
(1053, 171, 150.00, 9, '2020-03-11'),
(1054, 171, 150.00, 8, '2020-02-16'),
(1055, 171, 150.00, 8, '2020-02-17'),
(1056, 22, 130.00, 8, '2020-02-17'),
(1057, 22, 130.00, 8, '2020-02-18'),
(1058, 22, 130.00, 0, '2020-02-19'),
(1059, 22, 130.00, 0, '2020-02-20'),
(1060, 22, 130.00, 5, '2020-03-02'),
(1061, 22, 130.00, 5, '2020-03-03'),
(1062, 26, 44.00, 8, '2020-02-19'),
(1063, 26, 44.00, 8, '2020-02-20'),
(1064, 171, 150.00, 5, '2020-02-19'),
(1065, 171, 150.00, 5, '2020-02-20'),
(1066, 175, 130.00, 7, '2020-02-19'),
(1067, 175, 130.00, 7, '2020-02-20'),
(1068, 57, 222.00, 0, '2020-02-19'),
(1069, 57, 222.00, 0, '2020-02-20'),
(1070, 172, 166.00, 9, '2020-02-19'),
(1071, 172, 166.00, 9, '2020-02-20'),
(1072, 172, 166.00, 9, '2020-02-21'),
(1073, 172, 166.00, 9, '2020-02-22'),
(1074, 172, 166.00, 9, '2020-02-23'),
(1075, 172, 166.00, 9, '2020-02-24'),
(1076, 172, 166.00, 9, '2020-02-25'),
(1077, 172, 166.00, 9, '2020-02-26'),
(1078, 172, 166.00, 9, '2020-02-27'),
(1079, 172, 166.00, 9, '2020-02-28'),
(1080, 172, 166.00, 9, '2020-02-29'),
(1081, 22, 310.00, 0, '2020-02-21'),
(1082, 22, 310.00, 0, '2020-02-22'),
(1083, 22, 310.00, 0, '2020-02-23'),
(1084, 22, 310.00, 0, '2020-02-24'),
(1085, 22, 310.00, 0, '2020-02-25'),
(1086, 22, 310.00, 0, '2020-02-26'),
(1087, 22, 310.00, 0, '2020-02-27'),
(1088, 22, 310.00, 0, '2020-02-28'),
(1089, 22, 310.00, 0, '2020-02-29'),
(1090, 169, 121.00, 0, '2020-02-19'),
(1091, 169, 121.00, 0, '2020-02-20'),
(1092, 169, 121.00, 0, '2020-02-21'),
(1093, 169, 121.00, 0, '2020-02-22'),
(1094, 176, 130.00, 8, '2020-02-19'),
(1095, 176, 130.00, 8, '2020-02-20'),
(1096, 170, 128.00, 0, '2020-02-19'),
(1097, 170, 128.00, 0, '2020-02-20'),
(1098, 25, 250.00, 0, '2020-02-19'),
(1099, 25, 250.00, 0, '2020-02-20'),
(1100, 24, 130.00, 4, '2020-03-04'),
(1101, 24, 130.00, 4, '2020-03-05'),
(1102, 24, 130.00, 4, '2020-03-06'),
(1103, 24, 130.00, 4, '2020-03-07'),
(1104, 24, 130.00, 4, '2020-03-08'),
(1105, 24, 130.00, 4, '2020-03-09');

-- --------------------------------------------------------

--
-- Table structure for table `tblbintype`
--

CREATE TABLE `tblbintype` (
  `idBinType` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodeType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description2` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbintype`
--

INSERT INTO `tblbintype` (`idBinType`, `name`, `CodeType`, `description`, `description2`) VALUES
(1, 'General Waste', '1210', '<ul>\r\n	<li stlye=\"margin:1px;\">No asbestos or other hazardous material.</li>\r\n	<li stlye=\"margin:1px;\">No hardfill, concrete, sand, soil or bricks.</li>\r\n	<li stlye=\"margin:1px;\">No fibro fencing.</li>\r\n	<li stlye=\"margin:1px;\">No palm trunks, tree trunks roots or turf.</li>\r\n<li stlye=\"margin:1px;\">No wet paint tins.</li>\r\n</ul>', '<ul>\r\n	<li stlye=\"margin:1px;\">Light domestic waste.</li>\r\n	<li stlye=\"margin:1px;\">Light commercial waste.</li>\r\n	<li stlye=\"margin:1px;\">Household items, light office waste, paper and cardboard. </li>\r\n<li stlye=\"margin:1px;\">Furniture and appliances, e.g. cupboards, fridge’s dishwashers etc. </li>\r\n</ul>\r\n\r\n<strong>(NB: Mattresses and car/truck tyres will incur extra charges. If you wish to dispose of these please check with your supplier before placing them in the bin.)</strong>'),
(2, 'Mixed Heavy Waste', '1211', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No food products or food waste.</li>\r\n	<li>Tree trucks larger than 300mm.</li>\r\n</ul>', '<ul>\r\n	<li>Domestic, commercial, construction, renovation and demolition waste.</li>\r\n	<li>domestic, commercial, construction, renovation and demolition.</li>\r\n	<li>All types of appliances, furniture and green waste.</li>\r\n</ul>'),
(3, 'Clean Fills Waste', '1212', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>'),
(4, 'Green Waste', '1213', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No bricks, tiles, concrete, sand or soil.</li>\r\n	<li>No paper or cardboard.</li>\r\n	<li>No household waste or appliances.</li>\r\n<li>No food waste.</li>\r\n<li>No mattresses or tyres. </li>\r\n</ul>', '<ul>\r\n	<li>Green garden waste only.</li>\r\n	<li>Branches, leaves, weeds, bark with no soil.</li>\r\n	<li>Shrubs, grass clippings, twigs etc.</li>\r\n<li>Tree trunks smaller than 300mm.</li>\r\n</ul>\r\n\r\n<strong>(NB:This skip must be green waste only.)</strong>'),
(5, 'Soil/Dirt Waste', '1214', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No bricks, tiles, concrete, sand or soil.</li>\r\n	<li>No paper or cardboard.</li>\r\n	<li>No household waste or appliances.</li>\r\n<li>No food waste.</li>\r\n<li>No mattresses or tyres.</li>\r\n</ul>', '<ul>\r\n	<li>Strictly sand and soil only.</li>\r\n</ul>\r\n\r\n<strong>(NB:This skip must be sand/soil only. No other material is allowed.)</strong>');

-- --------------------------------------------------------

--
-- Table structure for table `tblbookingprice`
--

CREATE TABLE `tblbookingprice` (
  `idBookingPrice` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbookingprice`
--

INSERT INTO `tblbookingprice` (`idBookingPrice`, `price`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 5, '2019-05-21 04:11:07', '2019-05-20 18:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontact`
--

CREATE TABLE `tblcontact` (
  `id` int(11) NOT NULL,
  `first_name` int(11) NOT NULL,
  `last_name` int(11) NOT NULL,
  `email_address` int(11) NOT NULL,
  `delivery_address` int(11) NOT NULL,
  `organization` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `idCustomer` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbldelivery_address`
--

CREATE TABLE `tbldelivery_address` (
  `iddelivery_address` int(11) NOT NULL,
  `unit_number` varchar(100) NOT NULL,
  `lot_number` varchar(100) NOT NULL,
  `street_name` text NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `post_code` varchar(5) NOT NULL,
  `state` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `idproject_site` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldepot`
--

CREATE TABLE `tbldepot` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldepotvisits`
--

CREATE TABLE `tbldepotvisits` (
  `id` int(11) NOT NULL,
  `iddepot` int(11) NOT NULL,
  `departure_type` char(1) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldrivers`
--

CREATE TABLE `tbldrivers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hours` double DEFAULT NULL,
  `zone` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `licence` varchar(255) NOT NULL,
  `notes` text DEFAULT NULL,
  `onDuty` char(1) DEFAULT NULL,
  `email_address` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblgeo`
--

CREATE TABLE `tblgeo` (
  `id` int(11) NOT NULL,
  `idtask` int(11) NOT NULL,
  `start_latitude` varchar(100) NOT NULL,
  `start_longitude` varchar(100) NOT NULL,
  `onroute_latitude` double NOT NULL,
  `onroute_longitude` double NOT NULL,
  `stop_latitude` double NOT NULL,
  `stop_longitude` double NOT NULL,
  `open_start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `open_stop_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `onroute_start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `onroute_stop_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `onsite_start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `onsite_stop_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_open_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_onroute_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_onsite_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_total` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbljobcards`
--

CREATE TABLE `tbljobcards` (
  `id` int(11) NOT NULL,
  `delivery_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `primarycontact` varchar(255) DEFAULT NULL,
  `is_paid` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin_mobile`
--

CREATE TABLE `tbllogin_mobile` (
  `iddriver` int(11) NOT NULL,
  `username` varchar(11) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `permission` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderservice`
--

CREATE TABLE `tblorderservice` (
  `idOrderService` int(10) UNSIGNED NOT NULL,
  `idConsumer` int(11) NOT NULL,
  `idproject_sites` int(11) NOT NULL,
  `paymentUniqueCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalServiceCharge` double(8,2) DEFAULT NULL,
  `gst` double(8,2) DEFAULT NULL,
  `subtotal` double(8,2) DEFAULT NULL,
  `bookingfee` double(8,2) DEFAULT NULL,
  `deliveryComments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idSupplier` int(10) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `card_category` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_holder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `movetojobs` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_quote` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paid` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderstatus`
--

CREATE TABLE `tblorderstatus` (
  `idOrderStatus` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `status` char(1) NOT NULL COMMENT '1 = Paid 2= Accepted',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `operator` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblorder_items`
--

CREATE TABLE `tblorder_items` (
  `id` int(11) NOT NULL,
  `idorder` int(11) NOT NULL,
  `idbinservice` int(11) NOT NULL,
  `idsize` int(11) NOT NULL,
  `iddelivery_address` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `collection_date` date DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `attribute` char(1) DEFAULT NULL,
  `isbinupdate` char(1) DEFAULT NULL,
  `delivery_comments` text DEFAULT NULL,
  `unique_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblorganization`
--

CREATE TABLE `tblorganization` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `cashonly` char(1) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile_phone` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymentformtemp`
--

CREATE TABLE `tblpaymentformtemp` (
  `idPaymentForm` int(11) NOT NULL,
  `idPaymentTemp` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `special_note` text DEFAULT NULL,
  `paymentUniqueCode` varchar(20) DEFAULT NULL,
  `totalprice` float DEFAULT NULL,
  `gst` float NOT NULL,
  `subtotal` float NOT NULL,
  `bookingfee` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymenttemp`
--

CREATE TABLE `tblpaymenttemp` (
  `idPaymentTemp` int(11) NOT NULL,
  `idBinHire` int(11) NOT NULL,
  `deliveryDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `paid` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblproject_sites`
--

CREATE TABLE `tblproject_sites` (
  `id` int(11) NOT NULL,
  `idorganization` int(11) NOT NULL,
  `sitename` varchar(255) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `gate` varchar(100) NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblschedulestatus`
--

CREATE TABLE `tblschedulestatus` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblservicearea`
--

CREATE TABLE `tblservicearea` (
  `idArea` int(11) NOT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `area` text DEFAULT NULL,
  `parentareacode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblservicearea`
--

INSERT INTO `tblservicearea` (`idArea`, `zipcode`, `area`, `parentareacode`) VALUES
(1, '0000', 'Western Australia', '0'),
(2, '0001', 'North of The River', '0000'),
(3, '0002', 'South of The River', '0000'),
(4, '0003', 'Rockingham', '0000'),
(5, '0004', 'Mandurah\r\n', '0000'),
(6, '0005', 'Outer Metro', '0000'),
(7, '6064', 'ALEXANDER HEIGHTS', '0001'),
(8, '6038', 'ALKIMOS', '0001'),
(9, '6104', 'ASCOT', '0001'),
(10, '6065', 'ASHBY', '0001'),
(11, '6054', 'ASHFIELD', '0001'),
(12, '6069', 'AVELEY', '0001'),
(13, '6021', 'BALCATTA', '0001'),
(14, '6061', 'BALGA', '0001'),
(15, '6066', 'BALLAJURA', '0001'),
(16, '6031', 'BANKSIA GROVE', '0001'),
(17, '6056', 'BASKERVILLE', '0001'),
(18, '6054', 'BASSENDEAN', '0001'),
(19, '6053', 'BAYSWATER', '0001'),
(20, '6052', 'BEDFORD', '0001'),
(21, '6063', 'BEECHBORO', '0001'),
(22, '6027', 'BELDON', '0001'),
(23, '6069', 'BELHUS', '0001'),
(24, '6056', 'BELLEVUE', '0001'),
(25, '6104', 'BELMONT', '0001'),
(26, '6063', 'BENNET SPRINGS', '0001'),
(27, '6056', 'BOYA', '0001'),
(28, '6055', 'BRABHAM', '0001'),
(29, '6069', 'BRIGADOON', '0001'),
(30, '6084', 'BULLSBROOK', '0001'),
(31, '6028', 'BURNS BEACH', '0001'),
(32, '6100', 'BURSWOOD', '0001'),
(33, '6032', 'BUTLER', '0001'),
(34, '6033', 'CARABOODA', '0001'),
(35, '6020', 'CARINE', '0001'),
(36, '6101', 'CARLISLE', '0001'),
(37, '6055', 'CAVERSHAM', '0001'),
(38, '6556', 'CHIDLOW', '0001'),
(39, '6018', 'CHURCHLANDS', '0001'),
(40, '6015', 'CITY BEACH', '0001'),
(41, '6010', 'CLAREMONT', '0001'),
(42, '6030', 'CLARKSON', '0001'),
(43, '6105', 'CLOVERDALE', '0001'),
(44, '6027', 'CONNOLLY', '0001'),
(45, '6050', 'COOLBINIA', '0001'),
(46, '6025', 'CRAIGIE', '0001'),
(47, '6067', 'CULLACABARDEE', '0001'),
(48, '6028', 'CURRAMBINE', '0001'),
(49, '6008', 'DAGLISH', '0001'),
(50, '6065', 'DARCH', '0001'),
(51, '6070', 'DARLINGTON', '0001'),
(52, '6055', 'DAYTON', '0001'),
(53, '6059', 'DIANELLA', '0001'),
(54, '6018', 'DOUBLEVIEW', '0001'),
(55, '6023', 'DUNCRAIG', '0001'),
(56, '6004', 'EAST PERTH', '0001'),
(57, '6054', 'EDEN HILL', '0001'),
(58, '6027', 'EDGEWATER', '0001'),
(59, '6034', 'EGLINGTON', '0001'),
(60, '6069', 'ELLENBROOK', '0001'),
(61, '6062', 'EMBLETON', '0001'),
(62, '6014', 'FLOREAT', '0001'),
(63, '6058', 'FORRESTFIELD', '0001'),
(64, '6083', 'GIDGEGANNUP', '0001'),
(65, '6064', 'GIRRAWHEEN', '0001'),
(66, '6071', 'GLEN FORREST', '0001'),
(67, '6016', 'GLENDALOUGH', '0001'),
(68, '6065', 'GNANGARA', '0001'),
(69, '6076', 'GOOSEBERRY HILL', '0001'),
(70, '6056', 'GREENMOUNT', '0001'),
(71, '6024', 'GREENWOOD', '0001'),
(72, '6055', 'GUILDFORD', '0001'),
(73, '6018', 'GWELUP', '0001'),
(74, '6076', 'HACKETTS GULLY', '0001'),
(75, '6022', 'HAMERSLEY', '0001'),
(76, '6055', 'HAZELMERE', '0001'),
(77, '6027', 'HEATHRIDGE', '0001'),
(78, '6056', 'HELENA VALLEY', '0001'),
(79, '6055', 'HENLEY BROOK', '0001'),
(80, '6017', 'HERDSMAN', '0001'),
(81, '6056', 'HERNE HILL', '0001'),
(82, '6057', 'HIGH WYCOMBE', '0001'),
(83, '6003', 'HIGHGATE', '0001'),
(84, '6025', 'HILLARYS', '0001'),
(85, '6065', 'HOCKING', '0001'),
(86, '6071', 'HOVEA', '0001'),
(87, '6028', 'ILUKA', '0001'),
(88, '6052', 'INGLEWOOD', '0001'),
(89, '6018', 'INNALOO', '0001'),
(90, '6065', 'JANDABUP', '0001'),
(91, '6056', 'JANE BROOK', '0001'),
(92, '6032', 'JINDALEE', '0001'),
(93, '6014', 'JOLIMONT', '0001'),
(94, '6027', 'JOONDALUP', '0001'),
(95, '6060', 'JOONDANNA', '0001'),
(96, '6076', 'KALAMUNDA', '0001'),
(97, '6025', 'KALLAROO', '0001'),
(98, '6010', 'KARRAKATTA', '0001'),
(99, '6018', 'KARRINYUP', '0001'),
(100, '6105', 'KEWDALE', '0001'),
(101, '6054', 'KIARA', '0001'),
(102, '6026', 'KINGSLEY', '0001'),
(103, '6005', 'KINGS PARK', '0001'),
(104, '6028', 'KINROSS', '0001'),
(105, '6064', 'KOONDOOLA', '0001'),
(106, '6056', 'KOONGAMIA', '0001'),
(107, '6065', 'LANDSDALE', '0001'),
(108, '6100', 'LATHLAIN', '0001'),
(109, '6007', 'LEEDERVILLE', '0001'),
(110, '6065', 'LEXIA', '0001'),
(111, '6054', 'LOCKRIDGE', '0001'),
(112, '6065', 'MADELEY', '0001'),
(113, '6072', 'MAHOGANY CREEK', '0001'),
(114, '6057', 'MAIDA VALE', '0001'),
(115, '6090', 'MALAGA', '0001'),
(116, '6064', 'MARANGAROO', '0001'),
(117, '6065', 'MARIGINIUP', '0001'),
(118, '6020', 'MARMION', '0001'),
(119, '6051', 'MAYLANDS', '0001'),
(120, '6079', 'MELALEUCA', '0001'),
(121, '6050', 'MENORA', '0001'),
(122, '6030', 'MERRIWA', '0001'),
(123, '6056', 'MIDDLE SWAN', '0001'),
(124, '6056', 'MIDLAND', '0001'),
(125, '6056', 'MIDVALE', '0001'),
(126, '6056', 'MILLENDON', '0001'),
(127, '6030', 'MINDARIE', '0001'),
(128, '6061', 'MIRRABOOKA', '0001'),
(129, '6062', 'MORLEY', '0001'),
(130, '6082', 'MOUNT HELENA', '0001'),
(131, '6050', 'MOUNT LAWLEY', '0001'),
(132, '6010', 'MT CLAREMONT', '0001'),
(133, '6016', 'MT HAWTHORN', '0001'),
(134, '6027', 'MULLALOO', '0001'),
(135, '6073', 'MUNDARING', '0001'),
(136, '6009', 'NEDLANDS', '0001'),
(137, '6031', 'NEERABUP', '0001'),
(138, '6061', 'NOLLAMARA', '0001'),
(139, '6062', 'NORANDA', '0001'),
(140, '6020', 'NORTH BEACH', '0001'),
(141, '6006', 'NORTH PERTH', '0001'),
(142, '6003', 'NORTHBRIDGE', '0001'),
(143, '6032', 'NOWERGUP', '0001'),
(144, '6027', 'OCEAN REEF', '0001'),
(145, '6017', 'OSBORNE PARK', '0001'),
(146, '6025', 'PADBURY', '0001'),
(147, '6081', 'PARKERVILLE', '0001'),
(148, '6076', 'PAULS VALLEY', '0001'),
(149, '6065', 'PEARSALL', '0001'),
(150, '6000', 'PERTH', '0001'),
(151, '6105', 'PERTH AIRPORT', '0001'),
(152, '6076', 'PIESSE BROOK', '0001'),
(153, '6065', 'PINJAR', '0001'),
(154, '6030', 'QUINNS ROCKS', '0001'),
(155, '6056', 'RED HILL', '0001'),
(156, '6104', 'REDCLIFFE', '0001'),
(157, '6030', 'RIDGEWOOD', '0001'),
(158, '6103', 'RIVERVALE', '0001'),
(159, '6019', 'SCARBOROUGH', '0001'),
(160, '6008', 'SHENTON PARK', '0001'),
(161, '6065', 'SINAGRA', '0001'),
(162, '6020', 'SORRENTO', '0001'),
(163, '6055', 'STH GUILDFORD', '0001'),
(164, '6021', 'STIRLING', '0001'),
(165, '6056', 'STRATTON', '0001'),
(166, '6008', 'SUBIACO', '0001'),
(167, '6056', 'SWAN VIEW', '0001'),
(168, '6010', 'SWANBOURNE', '0001'),
(169, '6030', 'TAMALA PARK', '0001'),
(170, '6065', 'TAPPING', '0001'),
(171, '6069', 'THE VINES', '0001'),
(172, '6029', 'TRIGG', '0001'),
(173, '6060', 'TUART HILL', '0001'),
(174, '6037', 'TWO ROCKS', '0001'),
(175, '6069', 'UPPER SWAN', '0001'),
(176, '6100', 'VICTORIA PARK', '0001'),
(177, '6101', 'VIC PARK EAST', '0001'),
(178, '6056', 'VIVEASH', '0001'),
(179, '6076', 'WALLISTON', '0001'),
(180, '6065', 'WANGARA', '0001'),
(181, '6065', 'WANNEROO', '0001'),
(182, '6024', 'WARWICK', '0001'),
(183, '6020', 'WATERMANS BAY', '0001'),
(184, '6106', 'WELSHPOOL', '0001'),
(185, '6014', 'WEMBLEY', '0001'),
(186, '6019', 'WEMBLEY DOWNS', '0001'),
(187, '6007', 'WEST LEEDERVILLE', '0001'),
(188, '6005', 'WEST PERTH', '0001'),
(189, '6055', 'WEST SWAN', '0001'),
(190, '6061', 'WESTMINSTER', '0001'),
(191, '6068', 'WHITEMAN', '0001'),
(192, '6056', 'WOODBRIDGE', '0001'),
(193, '6018', 'WOODLANDS', '0001'),
(194, '6026', 'WOODVALE', '0001'),
(195, '6035', 'YANCHEP', '0001'),
(196, '6060', 'YOKINE', '0001'),
(197, '6154', 'ALFRED COVE', '0002'),
(198, '6153', 'APPLECROSS', '0002'),
(199, '6153', 'ARDROSS', '0002'),
(200, '6112', 'ARMADALE', '0002'),
(201, '6156', 'ATTADALE', '0002'),
(202, '6164', 'ATWELL', '0002'),
(203, '6164', 'AUBIN GROVE', '0002'),
(204, '6164', 'BANJUP', '0002'),
(205, '6150', 'BATEMAN', '0002'),
(206, '6162', 'BEACONSFIELD', '0002'),
(207, '6107', 'BECKENHAM', '0002'),
(208, '6112', 'BEDFORDALE', '0002'),
(209, '6164', 'BEELIAR', '0002'),
(210, '6102', 'BENTLEY', '0002'),
(211, '6163', 'BIBRA LAKE', '0002'),
(212, '6076', 'BICKLEY', '0002'),
(213, '6157', 'BICTON', '0002'),
(214, '6154', 'BOORAGOON', '0002'),
(215, '6153', 'BRENTWOOD', '0002'),
(216, '6112', 'BROOKDALE', '0002'),
(217, '6149', 'BULL CREEK', '0002'),
(218, '6111', 'CAMILLO', '0002'),
(219, '6111', 'CANNING MILLS', '0002'),
(220, '6155', 'CANNING VALE', '0002'),
(221, '6107', 'CANNINGTON', '0002'),
(222, '6076', 'CARMEL', '0002'),
(223, '6111', 'CHAMPION LAKES', '0002'),
(224, '6164', 'COCKBURN CENTRAL', '0002'),
(225, '6152', 'COMO', '0002'),
(226, '6163', 'COOLBELLUP', '0002'),
(227, '6011', 'COTTESLOE', '0002'),
(228, '6009', 'CRAWLEY', '0002'),
(229, '6009', 'DALKIETH', '0002'),
(230, '6107', 'EAST CANNINGTON', '0002'),
(231, '6158', 'EAST FREMANTLE', '0002'),
(232, '6101', 'EAST VICTORIA PARK', '0002'),
(233, '6148', 'FERNDALE', '0002'),
(234, '6112', 'FORRESTDALE', '0002'),
(235, '6160', 'FREMANTLE', '0002'),
(236, '6110', 'GOSNELLS', '0002'),
(237, '6163', 'HAMILTON HILL', '0002'),
(238, '6164', 'HAMMOND PARK', '0002'),
(239, '6112', 'HARRISDALE', '0002'),
(240, '6112', 'HAYNES', '0002'),
(241, '6112', 'HILBERT', '0002'),
(242, '6163', 'HILTON', '0002'),
(243, '6110', 'HUNTINGDALE', '0002'),
(244, '6164', 'JANDAKOT', '0002'),
(245, '6152', 'KARAWARA', '0002'),
(246, '6163', 'KARDINYA', '0002'),
(247, '6111', 'KELMSCOTT', '0002'),
(248, '6151', 'KENSINGTON', '0002'),
(249, '6107', 'KENWICK', '0002'),
(250, '6147', 'LANGFORD', '0002'),
(251, '6149', 'LEEMING', '0002'),
(252, '6076', 'LESMURDIE', '0002'),
(253, '6147', 'LYNWOOD', '0002'),
(254, '6109', 'MADDINGTON', '0002'),
(255, '6152', 'MANNING', '0002'),
(256, '6110', 'MARTIN', '0002'),
(257, '6156', 'MELVILLE', '0002'),
(258, '6012', 'MOSMAN PARK', '0002'),
(259, '6112', 'MOUNT NASURA', '0002'),
(260, '6153', 'MOUNT PLEASANT', '0002'),
(261, '6112', 'MOUNT RICHON', '0002'),
(262, '6150', 'MURDOCH', '0002'),
(263, '6154', 'MYAREE', '0002'),
(264, '6163', 'NORTH COOGEE', '0002'),
(265, '6163', 'NORTH LAKE', '0002'),
(266, '6159', 'NTH FREMANTLE', '0002'),
(267, '6163', 'O\'CONNOR', '0002'),
(268, '6109', 'ORANGE GROVE', '0002'),
(269, '6157', 'PALMYRA', '0002'),
(270, '6147', 'PARKWOOD', '0002'),
(271, '6011', 'PEPPERMINT GROVE', '0002'),
(272, '6112', 'PIARA WATERS', '0002'),
(273, '6107', 'QUEENS PARK', '0002'),
(274, '6148', 'RIVERTON', '0002'),
(275, '6111', 'ROLEYSTONE', '0002'),
(276, '6148', 'ROSSMOYNE', '0002'),
(277, '6152', 'SALTER POINT', '0002'),
(278, '6163', 'SAMSON', '0002'),
(279, '6112', 'SEVILLE GROVE', '0002'),
(280, '6148', 'SHELLEY', '0002'),
(281, '6164', 'SOUTH LAKE', '0002'),
(282, '6151', 'SOUTH PERTH', '0002'),
(283, '6110', 'SOUTHERN RIVER', '0002'),
(284, '6163', 'SPEARWOOD', '0002'),
(285, '6102', 'ST JAMES', '0002'),
(286, '6162', 'STH FREMANTLE', '0002'),
(287, '6164', 'SUCCESS', '0002'),
(288, '6108', 'THORNLIE', '0002'),
(289, '6152', 'WATERFORD', '0002'),
(290, '6107', 'WATTLE GROVE', '0002'),
(291, '6111', 'WESTFIELD', '0002'),
(292, '6162', 'WHITE GUM VALLEY', '0002'),
(293, '6156', 'WILLAGEE', '0002'),
(294, '6155', 'WILLETON', '0002'),
(295, '6107', 'WILSON', '0002'),
(296, '6150', 'WINTHROP', '0002'),
(297, '6112', 'WUNGONG', '0002'),
(298, '6164', 'YANGEBUP', '0002'),
(299, '6167', 'ANKETELL', '0003'),
(300, '6171', 'BALDIVIS', '0003'),
(301, '6167', 'BERTRAM', '0003'),
(302, '6122', 'BYFORD', '0003'),
(303, '6167', 'CALISTA', '0003'),
(304, '6122', 'CARDUP', '0003'),
(305, '6167', 'CASUARINA', '0003'),
(306, '6166', 'COOGEE', '0003'),
(307, '6168', 'COOLOONGUP', '0003'),
(308, '6122', 'DARLING DOWNS', '0003'),
(309, '6168', 'EAST ROCKINGHAM', '0003'),
(310, '6166', 'HENDERSON', '0003'),
(311, '6168', 'HILLMAN', '0003'),
(312, '6125', 'HOPELAND', '0003'),
(313, '6165', 'HOPE VALLEY', '0003'),
(314, '6124', 'JARRAHDALE', '0003'),
(315, '6122', 'KARRAKUP', '0003'),
(316, '6167', 'KWINANA', '0003'),
(317, '6170', 'LEDA', '0003'),
(318, '6167', 'MANDOGALUP', '0003'),
(319, '6125', 'MARDELLA', '0003'),
(320, '6167', 'MEDINA', '0003'),
(321, '6123', 'MUNDIJONG', '0003'),
(322, '6166', 'MUNSTER', '0003'),
(323, '6165', 'NAVAL BASE', '0003'),
(324, '6121', 'OAKFORD', '0003'),
(325, '6121', 'OLDBURY', '0003'),
(326, '6167', 'ORELIA', '0003'),
(327, '6167', 'PARMELIA', '0003'),
(328, '6168', 'PERON', '0003'),
(329, '6172', 'PORT KENNEDY', '0003'),
(330, '6167', 'POSTANS', '0003'),
(331, '6168', 'ROCKINGHAM', '0003'),
(332, '6169', 'SAFETY BAY', '0003'),
(333, '6125', 'SERPENTINE', '0003'),
(334, '6169', 'SHOALWATER', '0003'),
(335, '6167', 'THE SPECTACLES', '0003'),
(336, '6169', 'WAIKIKI', '0003'),
(337, '6167', 'WANDI', '0003'),
(338, '6169', 'WARNBRO', '0003'),
(339, '6166', 'WATTLEUP', '0003'),
(340, '6170', 'WELLARD', '0003'),
(341, '6123', 'WHITBY', '0003'),
(342, '6209', 'BARRAGUP', '0004'),
(343, '6211', 'BOUVARD', '0004'),
(344, '6210', 'COODANUP', '0004'),
(345, '6211', 'DAWESVILLE', '0004'),
(346, '6210', 'DUDLEY PARK', '0004'),
(347, '6210', 'ERSKINE', '0004'),
(348, '6208', 'FAIRBRIDGE', '0004'),
(349, '6210', 'FALCON', '0004'),
(350, '6209', 'FURNISSDALE', '0004'),
(351, '6174', 'GOLDEN BAY', '0004'),
(352, '6210', 'GREENFIELDS', '0004'),
(353, '6210', 'HALLS HEAD', '0004'),
(354, '6176', 'KARNUP', '0004'),
(355, '6182', 'KERALUP', '0004'),
(356, '6126', 'KEYSBROOK', '0004'),
(357, '6180', 'LAKELANDS', '0004'),
(358, '6210', 'MADORA BAY', '0004'),
(359, '6210', 'MANDURAH', '0004'),
(360, '6210', 'MEADOW SPRINGS', '0004'),
(361, '6207', 'NAMBEELUP', '0004'),
(362, '6208', 'NIRIMBA', '0004'),
(363, '6207', 'NORTH DANDALUP', '0004'),
(364, '6208', 'NORTH YUNDERUP', '0004'),
(365, '6180', 'PARKLANDS', '0004'),
(366, '6208', 'POINT GREY', '0004'),
(367, '6208', 'RAVENSWOOD', '0004'),
(368, '6210', 'SAN REMO', '0004'),
(369, '6173', 'SECRET HARBOUR', '0004'),
(370, '6210', 'SILVER SANDS', '0004'),
(371, '6175', 'SINGLETON', '0004'),
(372, '6208', 'SOUTH YUNDERUP', '0004'),
(373, '6181', 'STAKE HILL', '0004'),
(374, '6210', 'WANNANUP', '0004'),
(375, '6208', 'WEST PINJARRA', '0004'),
(376, '6208', 'YUNDERUP', '0004'),
(377, '6111', 'ASHENDON', '0005'),
(378, '6084', 'AVON VALLEY', '0005'),
(379, '6082', 'BAILUP', '0005'),
(380, '6562', 'BAKERS HILL', '0005'),
(381, '6556', 'BEECHINA', '0005'),
(382, '6214', 'BIRCHMONT', '0005'),
(383, '6208', 'BLYTHEWOOD', '0005'),
(384, '6556', 'GORRIE', '0005'),
(385, '6111', 'ILLAWARRA', '0005'),
(386, '6111', 'KARAGULLEN', '0005'),
(387, '6084', 'LOWER CHITTERING', '0005'),
(388, '6208', 'MEELON', '0005'),
(389, '6501', 'MUCHEA', '0005'),
(390, '6208', 'OAKLEY', '0005'),
(391, '6076', 'PICKERING BROOK', '0005'),
(392, '6076', 'RESERVOIR', '0005'),
(393, '6074', 'SAWYERS VALLEY', '0005'),
(394, '6081', 'STONEVILLE', '0005'),
(395, '6556', 'THE LAKES', '0005'),
(396, '6084', 'WALYUNGA NAT PARK', '0005'),
(397, '6041', 'WILBINGA', '0005'),
(398, '6558', 'WOORALOO', '0005');

-- --------------------------------------------------------

--
-- Table structure for table `tblsize`
--

CREATE TABLE `tblsize` (
  `idSize` int(10) UNSIGNED NOT NULL,
  `size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensions` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsize`
--

INSERT INTO `tblsize` (`idSize`, `size`, `dimensions`) VALUES
(1, '2 cubic metres', '2 Cubic Metres Skip is about the same capacity as 2 standard box trailers'),
(2, '3 cubic metres', '3 Cubic Metres Skip is about the same capacity as 3 standard box trailers'),
(3, '4 cubic metres', '4 Cubic Metre Skip is about the same capacity as 4 standard box trailerss'),
(4, '5 cubic metres', '5 Cubic Metre Skip is about the same capacity as 5 standard box trailers'),
(5, '6 cubic metres', '6 Cubic Metre Skip is about the same capacity as 6 standard box trailers'),
(6, '8 cubic metres', '8 Cubic Metres Skip is about the same capacity as 8 standard box trailers'),
(7, '10 cubic metres', '10 Cubic Metres Skip is about the same capacity as 12 standard box trailers'),
(10, '12 cubic metres', '12 Cubic Metres Skip is about the same capacity as 12 standard box trailers');

-- --------------------------------------------------------

--
-- Table structure for table `tblstatus`
--

CREATE TABLE `tblstatus` (
  `id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_html` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstatus`
--

INSERT INTO `tblstatus` (`id`, `status`, `status_html`) VALUES
(1, 'Open', '<span class=\'badge bg-default\'> Open </span>'),
(2, 'In Progress', '<span class=\'badge bg-info\'> In Progress </span>'),
(3, 'On Route', '<span class=\'badge bg-theme\'> On Ruote </span>'),
(5, 'Completed', '<span class=\'badge bg-warning\'> Completed </span>'),
(7, 'Paused', '<span class=\'badge bg-paused\'> Paused </span>'),
(4, 'On Site', '<span class=\'badge bg-success\'> Onsite </span>'),
(6, 'Cancel', '<span class=\'badge bg-danger\'> Cancel </span>');

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `idSupplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobilePhone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSaturday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSunday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mainServiceArea` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceContact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServicePhone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceMobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullAddress` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsupplier`
--

INSERT INTO `tblsupplier` (`idSupplier`, `name`, `contactName`, `phonenumber`, `email`, `mobilePhone`, `comments`, `email2`, `isOpenSaturday`, `isOpenSunday`, `mainServiceArea`, `customerServiceContact`, `customerServicePhone`, `customerServiceMobile`, `fullAddress`, `abn`) VALUES
(11, 'Webmaster', 'iHub', '0361241875', 'andrew.soms@gmail.com', '089658595043', 'test', 'andrew.soms@gmail.com', '1', '1', '2', 'Support', '0361 241822', '089658595066', 'Joondalup 6027', '44 331 419 320');

-- --------------------------------------------------------

--
-- Table structure for table `tbltasks`
--

CREATE TABLE `tbltasks` (
  `id` int(11) NOT NULL,
  `idjobcard` int(11) DEFAULT NULL,
  `job_type` int(11) DEFAULT NULL COMMENT '1=pick up 2=delivery',
  `starttime` timestamp NULL DEFAULT current_timestamp(),
  `endtime` timestamp NULL DEFAULT current_timestamp(),
  `assignedto` int(11) DEFAULT NULL,
  `taskname` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `actual_tasktime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `distance` double DEFAULT NULL,
  `actual_endtime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `orderitem` int(11) DEFAULT NULL,
  `assets` int(11) DEFAULT NULL,
  `driver` int(11) DEFAULT NULL,
  `start_latitude` double DEFAULT NULL,
  `start_longitude` double DEFAULT NULL,
  `onroute_latitude` double DEFAULT NULL,
  `onroute_longitude` double DEFAULT NULL,
  `stop_latitude` double DEFAULT NULL,
  `stop_longitude` double DEFAULT NULL,
  `active_task` int(11) DEFAULT NULL,
  `idorderitems` int(11) DEFAULT NULL,
  `driver_updated` char(1) DEFAULT NULL,
  `is_paid` char(1) DEFAULT NULL,
  `unique_id` varchar(100) DEFAULT NULL,
  `driver_notice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbltask_has_status`
--

CREATE TABLE `tbltask_has_status` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `total_time` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltask_has_time`
--

CREATE TABLE `tbltask_has_time` (
  `id` int(11) NOT NULL,
  `task_status_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltask_has_time`
--

INSERT INTO `tbltask_has_time` (`id`, `task_status_id`, `type_id`, `time`) VALUES
(360, 277, 2, '2020-05-28 01:50:00'),
(359, 278, 1, '2020-05-28 01:50:00'),
(358, 277, 2, '2020-05-28 01:50:00'),
(357, 277, 1, '2020-05-28 01:50:00'),
(356, 276, 1, '2020-05-27 23:05:00'),
(355, 275, 2, '2020-05-27 23:05:00'),
(354, 275, 1, '2020-05-27 22:25:00'),
(353, 274, 2, '2020-05-27 22:25:00'),
(352, 274, 1, '2020-05-27 22:25:00'),
(351, 273, 2, '2020-05-27 22:25:00'),
(350, 273, 1, '2020-05-27 22:15:00'),
(349, 272, 2, '2020-05-27 22:15:00'),
(348, 272, 1, '2020-05-27 22:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbltrucks`
--

CREATE TABLE `tbltrucks` (
  `id` int(11) NOT NULL,
  `truck_type` varchar(100) NOT NULL,
  `in_service` char(1) NOT NULL,
  `load_weight` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `registration` varchar(100) NOT NULL,
  `lift_type` varchar(100) NOT NULL,
  `lift_weight` varchar(100) NOT NULL,
  `next_service_date` date DEFAULT NULL,
  `onDuty` int(11) DEFAULT NULL,
  `iddriver` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminApproved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userStatus` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registerDate` date NOT NULL,
  `adminNotifications` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `idSupplier`, `username`, `password`, `isActive`, `role`, `adminApproved`, `userStatus`, `registerDate`, `adminNotifications`) VALUES
(5, 11, 'webmaster', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '1', '1', '1', '2018-06-01', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbluserservicearea`
--

CREATE TABLE `tbluserservicearea` (
  `idUserServiceArea` int(10) NOT NULL,
  `idUser` int(10) NOT NULL,
  `idSupplier` int(10) NOT NULL,
  `idServiceArea` int(10) NOT NULL,
  `serviceAreaParent` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluserservicearea`
--

INSERT INTO `tbluserservicearea` (`idUserServiceArea`, `idUser`, `idSupplier`, `idServiceArea`, `serviceAreaParent`) VALUES
(881, 5, 11, 7, '0001'),
(882, 5, 11, 8, '0001'),
(883, 5, 11, 9, '0001'),
(884, 5, 11, 10, '0001'),
(885, 5, 11, 11, '0001'),
(886, 5, 11, 12, '0001'),
(887, 5, 11, 13, '0001'),
(888, 5, 11, 14, '0001'),
(889, 5, 11, 15, '0001'),
(890, 5, 11, 16, '0001'),
(891, 5, 11, 17, '0001'),
(892, 5, 11, 18, '0001'),
(893, 5, 11, 19, '0001'),
(894, 5, 11, 20, '0001'),
(895, 5, 11, 21, '0001'),
(896, 5, 11, 22, '0001'),
(897, 5, 11, 23, '0001'),
(898, 5, 11, 24, '0001'),
(899, 5, 11, 25, '0001'),
(900, 5, 11, 26, '0001'),
(901, 5, 11, 27, '0001'),
(902, 5, 11, 28, '0001'),
(903, 5, 11, 29, '0001'),
(904, 5, 11, 30, '0001'),
(905, 5, 11, 31, '0001'),
(906, 5, 11, 32, '0001'),
(907, 5, 11, 33, '0001'),
(908, 5, 11, 34, '0001'),
(909, 5, 11, 35, '0001'),
(910, 5, 11, 36, '0001'),
(911, 5, 11, 37, '0001'),
(912, 5, 11, 38, '0001'),
(913, 5, 11, 39, '0001'),
(914, 5, 11, 40, '0001'),
(915, 5, 11, 41, '0001'),
(916, 5, 11, 42, '0001'),
(917, 5, 11, 43, '0001'),
(918, 5, 11, 44, '0001'),
(919, 5, 11, 45, '0001'),
(920, 5, 11, 46, '0001'),
(921, 5, 11, 47, '0001'),
(922, 5, 11, 48, '0001'),
(923, 5, 11, 49, '0001'),
(924, 5, 11, 50, '0001'),
(925, 5, 11, 51, '0001'),
(926, 5, 11, 52, '0001'),
(927, 5, 11, 53, '0001'),
(928, 5, 11, 54, '0001'),
(929, 5, 11, 55, '0001'),
(930, 5, 11, 56, '0001'),
(931, 5, 11, 57, '0001'),
(932, 5, 11, 58, '0001'),
(933, 5, 11, 59, '0001'),
(934, 5, 11, 60, '0001'),
(935, 5, 11, 61, '0001'),
(936, 5, 11, 62, '0001'),
(937, 5, 11, 63, '0001'),
(938, 5, 11, 64, '0001'),
(939, 5, 11, 65, '0001'),
(940, 5, 11, 66, '0001'),
(941, 5, 11, 67, '0001'),
(942, 5, 11, 68, '0001'),
(943, 5, 11, 69, '0001'),
(944, 5, 11, 70, '0001'),
(945, 5, 11, 71, '0001'),
(946, 5, 11, 72, '0001'),
(947, 5, 11, 73, '0001'),
(948, 5, 11, 74, '0001'),
(949, 5, 11, 75, '0001'),
(950, 5, 11, 76, '0001'),
(951, 5, 11, 77, '0001'),
(952, 5, 11, 78, '0001'),
(953, 5, 11, 79, '0001'),
(954, 5, 11, 80, '0001'),
(955, 5, 11, 81, '0001'),
(956, 5, 11, 82, '0001'),
(957, 5, 11, 83, '0001'),
(958, 5, 11, 84, '0001'),
(959, 5, 11, 85, '0001'),
(960, 5, 11, 86, '0001'),
(961, 5, 11, 87, '0001'),
(962, 5, 11, 88, '0001'),
(963, 5, 11, 89, '0001'),
(964, 5, 11, 90, '0001'),
(965, 5, 11, 91, '0001'),
(966, 5, 11, 92, '0001'),
(967, 5, 11, 93, '0001'),
(968, 5, 11, 94, '0001'),
(969, 5, 11, 95, '0001'),
(970, 5, 11, 96, '0001'),
(971, 5, 11, 97, '0001'),
(972, 5, 11, 98, '0001'),
(973, 5, 11, 99, '0001'),
(974, 5, 11, 100, '0001'),
(975, 5, 11, 101, '0001'),
(976, 5, 11, 102, '0001'),
(977, 5, 11, 103, '0001'),
(978, 5, 11, 104, '0001'),
(979, 5, 11, 105, '0001'),
(980, 5, 11, 106, '0001'),
(981, 5, 11, 107, '0001'),
(982, 5, 11, 108, '0001'),
(983, 5, 11, 109, '0001'),
(984, 5, 11, 110, '0001'),
(985, 5, 11, 111, '0001'),
(986, 5, 11, 112, '0001'),
(987, 5, 11, 113, '0001'),
(988, 5, 11, 114, '0001'),
(989, 5, 11, 115, '0001'),
(990, 5, 11, 116, '0001'),
(991, 5, 11, 117, '0001'),
(992, 5, 11, 118, '0001'),
(993, 5, 11, 119, '0001'),
(994, 5, 11, 120, '0001'),
(995, 5, 11, 121, '0001'),
(996, 5, 11, 122, '0001'),
(997, 5, 11, 123, '0001'),
(998, 5, 11, 124, '0001'),
(999, 5, 11, 125, '0001'),
(1000, 5, 11, 126, '0001'),
(1001, 5, 11, 127, '0001'),
(1002, 5, 11, 128, '0001'),
(1003, 5, 11, 129, '0001'),
(1004, 5, 11, 130, '0001'),
(1005, 5, 11, 131, '0001'),
(1006, 5, 11, 132, '0001'),
(1007, 5, 11, 133, '0001'),
(1008, 5, 11, 134, '0001'),
(1009, 5, 11, 135, '0001'),
(1010, 5, 11, 136, '0001'),
(1011, 5, 11, 137, '0001'),
(1012, 5, 11, 138, '0001'),
(1013, 5, 11, 139, '0001'),
(1014, 5, 11, 140, '0001'),
(1015, 5, 11, 141, '0001'),
(1016, 5, 11, 142, '0001'),
(1017, 5, 11, 143, '0001'),
(1018, 5, 11, 144, '0001'),
(1019, 5, 11, 145, '0001'),
(1020, 5, 11, 146, '0001'),
(1021, 5, 11, 147, '0001'),
(1022, 5, 11, 148, '0001'),
(1023, 5, 11, 149, '0001'),
(1024, 5, 11, 150, '0001'),
(1025, 5, 11, 151, '0001'),
(1026, 5, 11, 152, '0001'),
(1027, 5, 11, 153, '0001'),
(1028, 5, 11, 154, '0001'),
(1029, 5, 11, 155, '0001'),
(1030, 5, 11, 156, '0001'),
(1031, 5, 11, 157, '0001'),
(1032, 5, 11, 158, '0001'),
(1033, 5, 11, 159, '0001'),
(1034, 5, 11, 160, '0001'),
(1035, 5, 11, 161, '0001'),
(1036, 5, 11, 162, '0001'),
(1037, 5, 11, 163, '0001'),
(1038, 5, 11, 164, '0001'),
(1039, 5, 11, 165, '0001'),
(1040, 5, 11, 166, '0001'),
(1041, 5, 11, 167, '0001'),
(1042, 5, 11, 168, '0001'),
(1043, 5, 11, 169, '0001'),
(1044, 5, 11, 170, '0001'),
(1045, 5, 11, 171, '0001'),
(1046, 5, 11, 172, '0001'),
(1047, 5, 11, 173, '0001'),
(1048, 5, 11, 174, '0001'),
(1049, 5, 11, 175, '0001'),
(1050, 5, 11, 176, '0001'),
(1051, 5, 11, 177, '0001'),
(1052, 5, 11, 178, '0001'),
(1053, 5, 11, 179, '0001'),
(1054, 5, 11, 180, '0001'),
(1055, 5, 11, 181, '0001'),
(1056, 5, 11, 182, '0001'),
(1057, 5, 11, 183, '0001'),
(1058, 5, 11, 184, '0001'),
(1059, 5, 11, 185, '0001'),
(1060, 5, 11, 186, '0001'),
(1061, 5, 11, 187, '0001'),
(1062, 5, 11, 188, '0001'),
(1063, 5, 11, 189, '0001'),
(1064, 5, 11, 190, '0001'),
(1065, 5, 11, 191, '0001'),
(1066, 5, 11, 192, '0001'),
(1067, 5, 11, 193, '0001'),
(1068, 5, 11, 194, '0001'),
(1069, 5, 11, 195, '0001'),
(1070, 5, 11, 196, '0001'),
(1071, 5, 11, 342, '0004'),
(1072, 5, 11, 343, '0004'),
(1073, 5, 11, 344, '0004'),
(1074, 5, 11, 345, '0004'),
(1075, 5, 11, 346, '0004'),
(1076, 5, 11, 347, '0004'),
(1077, 5, 11, 348, '0004'),
(1078, 5, 11, 349, '0004'),
(1079, 5, 11, 350, '0004'),
(1080, 5, 11, 351, '0004'),
(1081, 5, 11, 352, '0004'),
(1082, 5, 11, 353, '0004'),
(1083, 5, 11, 354, '0004'),
(1084, 5, 11, 355, '0004'),
(1085, 5, 11, 356, '0004'),
(1086, 5, 11, 357, '0004'),
(1087, 5, 11, 358, '0004'),
(1088, 5, 11, 359, '0004'),
(1089, 5, 11, 360, '0004'),
(1090, 5, 11, 361, '0004'),
(1091, 5, 11, 362, '0004'),
(1092, 5, 11, 363, '0004'),
(1093, 5, 11, 364, '0004'),
(1094, 5, 11, 365, '0004'),
(1095, 5, 11, 366, '0004'),
(1096, 5, 11, 367, '0004'),
(1097, 5, 11, 368, '0004'),
(1098, 5, 11, 369, '0004'),
(1099, 5, 11, 370, '0004'),
(1100, 5, 11, 371, '0004'),
(1101, 5, 11, 372, '0004'),
(1102, 5, 11, 373, '0004'),
(1103, 5, 11, 374, '0004'),
(1104, 5, 11, 375, '0004'),
(1105, 5, 11, 376, '0004'),
(1106, 5, 11, 377, '0005'),
(1107, 5, 11, 378, '0005'),
(1108, 5, 11, 379, '0005'),
(1109, 5, 11, 380, '0005'),
(1110, 5, 11, 381, '0005'),
(1111, 5, 11, 382, '0005'),
(1112, 5, 11, 383, '0005'),
(1113, 5, 11, 384, '0005'),
(1114, 5, 11, 385, '0005'),
(1115, 5, 11, 386, '0005'),
(1116, 5, 11, 387, '0005'),
(1117, 5, 11, 388, '0005'),
(1118, 5, 11, 389, '0005'),
(1119, 5, 11, 390, '0005'),
(1120, 5, 11, 391, '0005'),
(1121, 5, 11, 392, '0005'),
(1122, 5, 11, 393, '0005'),
(1123, 5, 11, 394, '0005'),
(1124, 5, 11, 395, '0005'),
(1125, 5, 11, 396, '0005'),
(1126, 5, 11, 397, '0005'),
(1127, 5, 11, 398, '0005'),
(1128, 5, 11, 197, '0002'),
(1129, 5, 11, 198, '0002'),
(1130, 5, 11, 199, '0002'),
(1131, 5, 11, 200, '0002'),
(1132, 5, 11, 201, '0002'),
(1133, 5, 11, 202, '0002'),
(1134, 5, 11, 203, '0002'),
(1135, 5, 11, 204, '0002'),
(1136, 5, 11, 205, '0002'),
(1137, 5, 11, 206, '0002'),
(1138, 5, 11, 207, '0002'),
(1139, 5, 11, 208, '0002'),
(1140, 5, 11, 209, '0002'),
(1141, 5, 11, 210, '0002'),
(1142, 5, 11, 211, '0002'),
(1143, 5, 11, 212, '0002'),
(1144, 5, 11, 213, '0002'),
(1145, 5, 11, 214, '0002'),
(1146, 5, 11, 215, '0002'),
(1147, 5, 11, 216, '0002'),
(1148, 5, 11, 217, '0002'),
(1149, 5, 11, 218, '0002'),
(1150, 5, 11, 219, '0002'),
(1151, 5, 11, 220, '0002'),
(1152, 5, 11, 221, '0002'),
(1153, 5, 11, 222, '0002'),
(1154, 5, 11, 223, '0002'),
(1155, 5, 11, 224, '0002'),
(1156, 5, 11, 225, '0002'),
(1157, 5, 11, 226, '0002'),
(1158, 5, 11, 227, '0002'),
(1159, 5, 11, 228, '0002'),
(1160, 5, 11, 229, '0002'),
(1161, 5, 11, 230, '0002'),
(1162, 5, 11, 231, '0002'),
(1163, 5, 11, 232, '0002'),
(1164, 5, 11, 233, '0002'),
(1165, 5, 11, 234, '0002'),
(1166, 5, 11, 235, '0002'),
(1167, 5, 11, 236, '0002'),
(1168, 5, 11, 237, '0002'),
(1169, 5, 11, 238, '0002'),
(1170, 5, 11, 239, '0002'),
(1171, 5, 11, 240, '0002'),
(1172, 5, 11, 241, '0002'),
(1173, 5, 11, 242, '0002'),
(1174, 5, 11, 243, '0002'),
(1175, 5, 11, 244, '0002'),
(1176, 5, 11, 245, '0002'),
(1177, 5, 11, 246, '0002'),
(1178, 5, 11, 247, '0002'),
(1179, 5, 11, 248, '0002'),
(1180, 5, 11, 249, '0002'),
(1181, 5, 11, 250, '0002'),
(1182, 5, 11, 251, '0002'),
(1183, 5, 11, 252, '0002'),
(1184, 5, 11, 253, '0002'),
(1185, 5, 11, 254, '0002'),
(1186, 5, 11, 255, '0002'),
(1187, 5, 11, 256, '0002'),
(1188, 5, 11, 257, '0002'),
(1189, 5, 11, 258, '0002'),
(1190, 5, 11, 259, '0002'),
(1191, 5, 11, 260, '0002'),
(1192, 5, 11, 261, '0002'),
(1193, 5, 11, 262, '0002'),
(1194, 5, 11, 263, '0002'),
(1195, 5, 11, 264, '0002'),
(1196, 5, 11, 265, '0002'),
(1197, 5, 11, 266, '0002'),
(1198, 5, 11, 267, '0002'),
(1199, 5, 11, 268, '0002'),
(1200, 5, 11, 269, '0002'),
(1201, 5, 11, 270, '0002'),
(1202, 5, 11, 271, '0002'),
(1203, 5, 11, 272, '0002'),
(1204, 5, 11, 273, '0002'),
(1205, 5, 11, 274, '0002'),
(1206, 5, 11, 275, '0002'),
(1207, 5, 11, 276, '0002'),
(1208, 5, 11, 277, '0002'),
(1209, 5, 11, 278, '0002'),
(1210, 5, 11, 279, '0002'),
(1211, 5, 11, 280, '0002'),
(1212, 5, 11, 281, '0002'),
(1213, 5, 11, 282, '0002'),
(1214, 5, 11, 283, '0002'),
(1215, 5, 11, 284, '0002'),
(1216, 5, 11, 285, '0002'),
(1217, 5, 11, 286, '0002'),
(1218, 5, 11, 287, '0002'),
(1219, 5, 11, 288, '0002'),
(1220, 5, 11, 289, '0002'),
(1221, 5, 11, 290, '0002'),
(1222, 5, 11, 291, '0002'),
(1223, 5, 11, 292, '0002'),
(1224, 5, 11, 293, '0002'),
(1225, 5, 11, 294, '0002'),
(1226, 5, 11, 295, '0002'),
(1227, 5, 11, 296, '0002'),
(1228, 5, 11, 297, '0002'),
(1229, 5, 11, 298, '0002');

-- --------------------------------------------------------

--
-- Table structure for table `user_confirmation`
--

CREATE TABLE `user_confirmation` (
  `idConfirmation` int(11) NOT NULL,
  `idSupplier` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `csv_data`
--
ALTER TABLE `csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  ADD PRIMARY KEY (`idNonDeliveryDays`);

--
-- Indexes for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  ADD PRIMARY KEY (`idBinService`);

--
-- Indexes for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  ADD PRIMARY KEY (`idBinServiceOptions`);

--
-- Indexes for table `tblbinservicestok`
--
ALTER TABLE `tblbinservicestok`
  ADD PRIMARY KEY (`idBinServiceStok`);

--
-- Indexes for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  ADD PRIMARY KEY (`idBinServiceUpdates`);

--
-- Indexes for table `tblbintype`
--
ALTER TABLE `tblbintype`
  ADD PRIMARY KEY (`idBinType`);

--
-- Indexes for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  ADD PRIMARY KEY (`idBookingPrice`);

--
-- Indexes for table `tblcontact`
--
ALTER TABLE `tblcontact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `tbldelivery_address`
--
ALTER TABLE `tbldelivery_address`
  ADD PRIMARY KEY (`iddelivery_address`);

--
-- Indexes for table `tbldepot`
--
ALTER TABLE `tbldepot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbldrivers`
--
ALTER TABLE `tbldrivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblgeo`
--
ALTER TABLE `tblgeo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbljobcards`
--
ALTER TABLE `tbljobcards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  ADD PRIMARY KEY (`idOrderService`);

--
-- Indexes for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  ADD PRIMARY KEY (`idOrderStatus`);

--
-- Indexes for table `tblorder_items`
--
ALTER TABLE `tblorder_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblorganization`
--
ALTER TABLE `tblorganization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  ADD PRIMARY KEY (`idPaymentForm`);

--
-- Indexes for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  ADD PRIMARY KEY (`idPaymentTemp`);

--
-- Indexes for table `tblproject_sites`
--
ALTER TABLE `tblproject_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblschedulestatus`
--
ALTER TABLE `tblschedulestatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  ADD PRIMARY KEY (`idArea`);

--
-- Indexes for table `tblsize`
--
ALTER TABLE `tblsize`
  ADD PRIMARY KEY (`idSize`);

--
-- Indexes for table `tblstatus`
--
ALTER TABLE `tblstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `tbltasks`
--
ALTER TABLE `tbltasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltask_has_status`
--
ALTER TABLE `tbltask_has_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltask_has_time`
--
ALTER TABLE `tbltask_has_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltrucks`
--
ALTER TABLE `tbltrucks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  ADD PRIMARY KEY (`idUserServiceArea`);

--
-- Indexes for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  ADD PRIMARY KEY (`idConfirmation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `csv_data`
--
ALTER TABLE `csv_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  MODIFY `idNonDeliveryDays` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  MODIFY `idBinService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  MODIFY `idBinServiceOptions` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tblbinservicestok`
--
ALTER TABLE `tblbinservicestok`
  MODIFY `idBinServiceStok` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1017;

--
-- AUTO_INCREMENT for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  MODIFY `idBinServiceUpdates` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1106;

--
-- AUTO_INCREMENT for table `tblbintype`
--
ALTER TABLE `tblbintype`
  MODIFY `idBinType` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  MODIFY `idBookingPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcontact`
--
ALTER TABLE `tblcontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldelivery_address`
--
ALTER TABLE `tbldelivery_address`
  MODIFY `iddelivery_address` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbldepot`
--
ALTER TABLE `tbldepot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldrivers`
--
ALTER TABLE `tbldrivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblgeo`
--
ALTER TABLE `tblgeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbljobcards`
--
ALTER TABLE `tbljobcards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  MODIFY `idOrderService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  MODIFY `idOrderStatus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tblorder_items`
--
ALTER TABLE `tblorder_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tblorganization`
--
ALTER TABLE `tblorganization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  MODIFY `idPaymentForm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=339;

--
-- AUTO_INCREMENT for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  MODIFY `idPaymentTemp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=464;

--
-- AUTO_INCREMENT for table `tblproject_sites`
--
ALTER TABLE `tblproject_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblschedulestatus`
--
ALTER TABLE `tblschedulestatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=399;

--
-- AUTO_INCREMENT for table `tblsize`
--
ALTER TABLE `tblsize`
  MODIFY `idSize` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblstatus`
--
ALTER TABLE `tblstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  MODIFY `idSupplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbltasks`
--
ALTER TABLE `tbltasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tbltask_has_status`
--
ALTER TABLE `tbltask_has_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `tbltask_has_time`
--
ALTER TABLE `tbltask_has_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=386;

--
-- AUTO_INCREMENT for table `tbltrucks`
--
ALTER TABLE `tbltrucks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  MODIFY `idUserServiceArea` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1230;

--
-- AUTO_INCREMENT for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  MODIFY `idConfirmation` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
