<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbltrucks extends Model
{
	protected $table = 'tbltrucks';
	public $timestamps = false;
}
