<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblbinnondelivery extends Model
{
	protected $table = 'tblbinnondelivery';
	public $timestamps = false;
}
