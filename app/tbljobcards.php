<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbljobcards extends Model
{
	protected $table = 'tbljobcards';
	public $timestamps = false;
}
