<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\tblbintype;
use App\tblsize;
use App\tblservicearea;
use Validator;
use App\Transformer\bintypeTransformer;
use App\Transformer\sizeTransformer;
use App\Transformer\binhireTransformer;
use App\Transformer\stateTransformer;
use App\Transformer\binhireoptionsTransformer;
use App\Transformer\binhire_dailyTransformer;
use App\Transformer\paymentTransformer;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
	protected $respose;
	protected $custZipCode;
	public function __construct(Response $response){
		$this->response = $response;
	}
}
