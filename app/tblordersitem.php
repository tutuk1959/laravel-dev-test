<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblordersitem extends Model
{
	protected $table = 'tblorder_items';
	public $timestamps = false;
}
