<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class binhireoptionsTransformer extends TransformerAbstract {
 
    public function transform($binhire) {
        return [
            'idBinServiceOptions' => $binhire->idBinServiceOptions,
            'extraHireagePrice' => $binhire->extraHireagePrice,
            'extraHireageDays' => $binhire->extraHireageDays,
            'excessWeightPrice' => $binhire->excessWeightPrice,
        ];
    }
 }