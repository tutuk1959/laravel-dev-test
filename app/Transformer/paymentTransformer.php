<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class paymentTransformer extends TransformerAbstract {
 
    public function transform($id) {
    	$url = url('paymentdetails/');
        return [
            'idpaymenttemp' => $id,
            'url' => $url,
        ];
    }
 }